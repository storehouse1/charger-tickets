# Pulls out specific columns from Jira ticket export and prints to new csv
#     Copyright (C) 2022  James Robertson
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import csv
from datetime import date
from tkinter import filedialog

if __name__ == '__main__':

    # Get input file
    csv_file = filedialog.askopenfilename()
    df = pd.read_csv(csv_file)
    summary = df.Summary

    # Sort out needed fields
    ticket_id = df['Issue key']
    reporter = df.Reporter
    charger_type = df['Custom field (Charger Type)']
    school = df['Custom field (School)']
    student_id = df['Custom field (Student ID)']
    student_name = df['Custom field (Student Name)']
    description = df['Description']  # only needed for type detection
    device_request = df['Custom field (Other Request)']  # type detection for most request for device tickets

    # define words that indicate charger type
    barrel_words = ["barrel", "round", "circle"]
    usb_words = ["flat", "usb-c", "usb"]

    # Set date
    today = date.today()

    # Write new csv file
    with open("output.csv", 'w', newline='') as f:
        writer = csv.writer(f, dialect='excel')

        for i in range(0, len(summary)):
            # check for charger type in summary or description if no charger type given
            if str(charger_type[i] == "nan"):
                if any(word in str(summary[i]).casefold() for word in barrel_words) \
                        or any(word in str(description[i]).casefold() for word in barrel_words) \
                        or any(word in str(device_request[i]).casefold() for word in barrel_words):
                    charger_type[i] = "Barrel (Round)"

                if any(word in str(summary[i]).casefold() for word in usb_words) \
                        or any(word in str(description[i]).casefold() for word in usb_words) \
                        or any(word in str(device_request[i]).casefold() for word in usb_words):
                    charger_type[i] = "USB-C (Flat)"

            data = [str(today), str(summary[i]), str(ticket_id[i]), str(reporter[i]), str(charger_type[i]),
                    str(school[i]), str(student_id[i]), str(student_name[i])]
            writer.writerow(data)
