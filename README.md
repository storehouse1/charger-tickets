# charger-tickets

Gets the needed info from a Jira export to create labels for the daily charger requests.

## Running the script

An executable of this script can be found here: https://drive.google.com/drive/folders/154TSki-4n-bluE9mDGHUZ04o01h8l7l4?usp=share_link
This is needed for the script to work easily on district computers. The script can also be run as a python program, but the `pandas` package will need to be installed.

## Generating new executable

The executable for this script was generated using pyinstaller with the command `pyinstaller -F -n cahrgers main.py` This will require installing the `pyinstaller` package.
